@extends('layout.master')
@section('title')
Tambah Cast
@endsection
@section('content')
<h1>{{$cast->nama}} Berumur {{$cast->umur}} Tahun</h1>
<h3>Bio :</h3>
<p>{{$cast->bio}}</p>
<br>
<a href="/cast" class="btn btn-success">Back</a>
@endsection