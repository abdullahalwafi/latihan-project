@extends('layout.master')
@section('title')
Tambah genre
@endsection
@section('content')
<form method="post" action="/genre/{{$genre->id}}">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama" class="form-control" value="{{$genre->nama}}" >
    </div>
    @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
    <a href="/genre" class="btn btn-success">Back</a>

  </form>
@endsection