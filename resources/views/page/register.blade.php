@extends('layout.master')
@section('title')
Halaman Form
@endsection
@section('content')
<h1>Buat Account Baru</h1>
<h3>Sign Up Form</h3>
<form action="/kirim" method="post">
    @csrf
    <label for="">First name :</label><br>
    <input type="text" name="first_name"><br><br>

    <label for="">Last name :</label><br>
    <input type="text" name="last_name"><br><br>

    <label for="">Gender</label><br>
    <input type="radio" name="gender" value="male"> Male <br>
    <input type="radio" name="gender" value="female"> female<br><br>

    <label for="">Nationality</label><br>
    <select name="nationality">
        <option value="indonesia">Indonesia</option>
        <option value="Malaysia">Malaysia</option>
        <option value="Singapura">Singapura</option>
        <option value="Thailand">Thailand</option>
        <option value="Filipina">Filipina</option>
        <option value="Kamboja">Kamboja</option>
        <option value="Brunei Darassulam">Brunei Darassulam</option>
    </select><br><br>

    <label for="">Language Spoken</label><br>
    <input type="checkbox" name="language"> Bahasa Indonesia <br>
    <input type="checkbox" name="language"> Englist <br>
    <input type="checkbox" name="language"> Other<br><br>

    <label for="">Bio</label><br>
    <textarea name="bio" cols="30" rows="10"></textarea><br><br>
    <input type="submit" name="signup" value="Sign Up">
</form>
@endsection