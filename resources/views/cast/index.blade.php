@extends('layout.master')
@section('title')
Data Cast
@endsection
@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css" />
@endpush
@section('content')
    <a href="/cast/create" class="btn btn-primary mb-2">Tambah Cast</a>
    <!-- /.card-header -->
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Umur</th>
                    <th>Bio</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($cast as $key => $item)
                <tr>
                    <td>{{$key + 1}} </td>
                    <td>{{$item->nama}} </td>
                    <td>{{$item->umur}} </td>
                    <td>{{$item->bio}} </td>
                    <td>
                        <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <form class="mb-2" method="post" action="/cast/{{$item->id}}">
                            @csrf
                            @method('delete')
                            <input onclick="return confirm('Yakin Di Hapus?')" type="submit" value="Delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="5"style="text-align:center;">Data Tidak Ada!</td>
                </tr>
                @endforelse
            </tbody>
        </table>
@endsection
@push('script')
<script src="{{asset('template/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('template/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
    $(function() {
        $("#example1").DataTable();
    });
</script>
@endpush
