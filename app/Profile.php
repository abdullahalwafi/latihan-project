<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = "Profile";
    protected $fillable = ["umur", "alamat", "bio", "user_id"];
}
