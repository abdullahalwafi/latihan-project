<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\IndexController;

Route::get('/', 'IndexController@index');
Route::get('/register', 'AuthController@register');
Route::post('kirim/', 'AuthController@welcome');
Route::get('/data-tables', 'IndexController@table');

// CRUD CAST

Route::get('/cast/create', 'CastController@create'); //mengarah ke tambah cast
Route::post('/cast', 'CastController@store'); //menyimpan data form ke database

Route::get('/cast', 'CastController@index'); // menampilkan data

Route::get('/cast/{cast_id}', 'CastController@show'); //menampilkan data

Route::get('/cast/{cast_id}/edit', 'CastController@edit'); // menampilkan edit
Route::put('/cast/{cast_id}', 'CastController@update'); // menampilkan update

Route::delete('/cast/{cast_id}', 'CastController@destroy'); //untuk delete


// CRUD GENRE
Route::get('/genre/create', 'GenreController@create'); //mengarah ke tambah cast
Route::post('/genre', 'GenreController@store'); //menyimpan data form ke database

Route::get('/genre', 'GenreController@index'); // menampilkan data

Route::get('/genre/{genre_id}', 'GenreController@show'); //menampilkan data

Route::get('/genre/{genre_id}/edit', 'GenreController@edit'); // menampilkan edit
Route::put('/genre/{genre_id}', 'GenreController@update'); // menampilkan update

Route::delete('/genre/{genre_id}', 'GenreController@destroy'); //untuk delete


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
