@extends('layout.master')
@section('title')
Tambah Cast
@endsection
@section('content')
<form method="post" action="/cast/{{$cast->id}}">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama" class="form-control" value="{{$cast->nama}}" >
    </div>
    @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Umur</label>
      <input type="number" name="umur" class="form-control" value="{{$cast->umur}}" >
    </div>
    @error('umur')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Bio</label>
        <textarea name="bio" class="form-control" value="{{$cast->bio}}" >{{$cast->bio}}</textarea>
    </div>
    @error('bio')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
    <a href="/cast" class="btn btn-success">Back</a>

  </form>
@endsection